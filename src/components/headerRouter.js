import React, { useState, useEffect, useContext } from 'react';
import ScreenTransfer from './screenTransfer'
import ScreenLogin from './screenLogin'
import userContext from "./../context/userContext"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import { useHistory, withRouter, useLocation } from "react-router-dom";

function HeaderRouter(props) {
    const [context, setContext] = useContext(userContext);
    const history = useHistory()
    const location = useLocation();
    const logOut = async (event) => {
        console.log("logOut")
        setContext({"name": ""})
      }
    useEffect(() => {
        //console.log("history",history.location.pathname)
        console.log("pathname", location.pathname);
    });
    return (
        <>
            <li>
              <span className={"navbar-brand " + (location.pathname=="/"?"active":"")} to="/">Company</span>
            </li>
            <li className={(context.name!=="" ? '': "display") + (location.pathname=="/Home"?"active":"")}>
                <Link to="/Home">Home</Link>
            </li>
            <li className={(context.name!=="" ? '': "display" ) + (location.pathname=="/Transfer"?"active":"")}>
                <Link to="/Transfer">Transfer</Link>
            </li>
            <li className={(context.name!=="" ? 'right': "right display") +  (location.pathname=="/"?"active":"")}>
                <Link to="/" onClick={logOut}>Log Out</Link>
            </li>
        </>
    )

}
export default HeaderRouter