import React, { useState, useEffect, useContext } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './header.css';
import ScreenHome from './screenHome'
import ScreenTransfer from './screenTransfer'
import ScreenLogin from './screenLogin'
import userContext from "./../context/userContext"
import { useHistory, withRouter, useLocation } from "react-router-dom";
import HeaderRouter from './headerRouter'

function Header(props) {
    const [context, setContext] = useContext(userContext);
    ////const location = useLocation()
    //const history = useHistory()
    return (
      <>
        <Router>
          <ul className="titulos">
            <HeaderRouter/>
          </ul>
          <Switch>
            <Route path="/Home">
              <ScreenHome />
            </Route>
            <Route path="/Transfer">
              <ScreenTransfer />
            </Route>
            <Route path="/">
              <ScreenLogin />
            </Route>
          </Switch>
        </Router>
      </>
    )
}
export default Header