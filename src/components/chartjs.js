import React, { useState, useEffect, useContext, useRef } from 'react';
import userContext from "./../context/userContext"
import Chart from 'chart.js'

function ChartJs({data}) {

    const chartRef = useRef(null);

    useEffect(() => {
        console.log("ChartJs============useEffect")
        const myChartRef = chartRef.current.getContext('2d');
        console.log(myChartRef)
        console.log(data)
        const o_chart = new Chart(myChartRef, data);
        o_chart.update();
    },[data]);

    return (
        <canvas id="myChart" width="400" height="400" ref={chartRef} ></canvas>
    )
}
export default ChartJs