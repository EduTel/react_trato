import logo from './logo.svg';
import './App.css';
import Header from './components/header'
import React, { useState, useEffect, useMemo } from 'react';
import userContext from "./context/userContext"

function App() {
  const [context, setContext] = useState({name: "",acount: ""});
  return (
    <userContext.Provider value={[context, setContext]}>
      <Header/>
    </userContext.Provider>
  );
}

export default App;
