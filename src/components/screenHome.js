import React, { useEffect, useContext, useState } from 'react';
import userContext from "./../context/userContext"
import './screenHome.css';
import './template.css';
import Chartjsgroupby from './chartjsgroupby'

function ScreenHome(props) {
    const [context, setContext] = useContext(userContext);
    const [table, setTable] = useState([]);

    // De forma similar a componentDidMount y componentDidUpdate
    useEffect(() => {
        console.log("ScreenHome============useEffect")
        const url = "https://react-trato-api.herokuapp.com/AccountBalance"
        fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log("data 0",data)
            setTable(data)
            console.log("data 1",data)
        });
    },[]);
    return (
        <>
            <h1 className="title">welcome to you online banking {context?.name}</h1>
            <div className="col-4 card size_card-4">
                <Chartjsgroupby/>
                <p className="titlaCard">Transaction History</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div >
            <div className="col-4 card size_card-4">
                <img src="https://via.placeholder.com/320x200" className="responsive center" alt="Girl in a jacket"/>
                <p className="titlaCard">Main expenses</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <div className="col-4 card size_card-4">
                <p className="titlaCard">Current Balance</p>
                {
                    <table className="table" style={ {width : "100%"}}>
                        <tr>
                            <th>account</th>
                            <th>balance</th>
                            <th>Date of lastest Transfer</th>
                        </tr>
                        {
                            table?.balance?.map((data, index)=>{
                                return <tr key={index}><td>{data.account}</td><td>{data.balance.currency+""+data.balance.value}</td><td>-----</td></tr>
                            })
                        }
                    </table>
                }
            </div>
        </>
    )
}
export default ScreenHome