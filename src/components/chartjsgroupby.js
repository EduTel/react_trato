import React, { useState, useEffect, useContext, useRef } from 'react';
import userContext from "./../context/userContext"
import ChartJs from './chartjs'
import { goup_by_currency, get_data_chart_js } from './library/request_trannform'

function Chartjsgroupby({data}) {

    const [data_chart, setDataChart] = useState({});

    useEffect(() => {
        console.log("ScreenHome============useEffect")
        const url = `https://react-trato-api.herokuapp.com/History/`
        fetch(url).then(response => {
            return response.json()
        }).then(data => {
            console.log(data)
            let result = get_data_chart_js(goup_by_currency(data))
            console.log(result)
            const myData = {
                type: "pie",
                data: {
                    labels: [],
                    datasets: [{
                        data: [],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                }
            }
            myData["data"]["labels"] = result["labels"]
            myData["data"]["datasets"][0]["data"] = result["data"]
            console.log(myData)
            setDataChart(myData)
        });
    },[]);

    return (
        <>
            <ChartJs data={data_chart} />
        </>
    )
}
export default Chartjsgroupby