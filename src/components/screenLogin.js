import React, { useState, useEffect, useMemo, useContext } from 'react';
import './screenLogin.css';
import userContext from "./../context/userContext"
import { useHistory } from "react-router-dom";

let default_formulario_data = {
    "username": "",
    "password": "",
}
let error_formulario_data = {
    "username": false,
    "password": false,
}
function ScreenLogin(props) {
    const [context, setContext] = useContext(userContext);
    const [submit, setSubmit] = useState(false);
    const [formulario, setFormulario] = useState(default_formulario_data);
    const [error_formulario, setErrorFormulario] = useState(error_formulario_data);
    let history = useHistory();
    const onChange_setFormulario = (event) => {
        console.log("onChange_setFormulario")
        //console.log(event)
        setFormulario({
            ...formulario,
            [event.target.name]: event.target.value
        });
        f_validation();
    }
    const f_validation = async (event) => {
        console.log("f_validation")
        await setTimeout(function(){
            //console.log("error_formulario 0 ",error_formulario)
            let aux = false;
            if( /^[a-zA-Z0-9!"$%&/]{8,20}$/.test(String(formulario.username.trim())) ){
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "username": false
                }))
            }else{
                aux = true
                console.log("error 0")
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "username": true
                }))
            }
            if( /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z0-9!"$%&/]{8,20}$/.test(String(formulario.password.trim())) ){
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "password": false
                }))
            }else{
                aux = true
                console.log("error 1")
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "password": true
                }))
            }
            setSubmit(!aux)
            //console.log("error_formulario 1", error_formulario)
        }, 1000);
    }
    useEffect(() => {
        if(error_formulario){
            console.log(Object.values(error_formulario))
        }
        if(context){
            console.log("context",context)
        }
    }, [context,error_formulario])

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("=================handleSubmit")
        setContext({"name": formulario.username.trim(), acount: 123456789})
        console.log(history)
        history.push("/Home")
    }
    return (
        <>
            <div className="vertical-center">
                <div className="form-login">
                    <div className="vh-35 center">
                        <span className="title-form">Login</span>
                    </div>
                    <div className="vh-35">
                        <form onSubmit={handleSubmit}>
                            <input className="" type="text" id="username" name="username"  placeholder="asdasdasd" onChange={onChange_setFormulario} onBlur={()=>f_validation()}/>
                            {error_formulario.username && <span>error</span>}
                            <input className="" type="text" id="password" name="password"  placeholder="1234567A!a" onChange={onChange_setFormulario} onBlur={()=>f_validation()}/>
                            {error_formulario.password && <span>error</span>}
                            <div className="center">
                                {
                                    submit? <button className="btn-login" type="submit">Enter</button> : <button className="btn-login disabled" disabled type="submit">Enter</button>
                                }
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </>
    )
}
export default ScreenLogin