const goup_by_currency = (data=[])=>{
    const group_toAccount = {}
    data.transactions.forEach(function logArrayElements(element, index, array) {
        if(group_toAccount[element.toAccount] === undefined){
            group_toAccount[element.toAccount] = { [element?.amount?.currency]: [element?.amount?.value]}
        }else{
            //console.log("toAccount ",group_toAccount[element.toAccount])
            if(group_toAccount[element.toAccount][element?.amount?.currency] === undefined){
                group_toAccount[element.toAccount][element?.amount?.currency] = [element?.amount?.value]
            }else{
                group_toAccount[element.toAccount][element?.amount?.currency].push(element?.amount?.value)
            }
        }
    })
    return group_toAccount
}
const get_data_chart_js =  (data=[]) => {
    console.log(data)
    let data_return ={
        labels: [],
        data: []
    }
    Object.keys(data).forEach(function logArrayElements(element1, index, array) {
        Object.keys(data[element1]).forEach(function logArrayElements(element2, index, array) {
            data_return["labels"].push(element1+"_"+element2)
            data_return["data"].push(data[element1][element2].reduce((a, b) => a + b))
        })
    })
    return data_return
}
export { goup_by_currency, get_data_chart_js };