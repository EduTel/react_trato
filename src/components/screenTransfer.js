import React, { useState, useEffect } from 'react';
import Chartjsgroupby from './chartjsgroupby'
import "./screenTransfer.css"
let default_formulario_data = {
    "fromAccount":"",
    "toAccount": "",
    "amount": ""
}
let error_formulario_data = {
    "toAccount": false,
    "amount": false
}
function ScreenTransfer(props) {
    const [submit, setSubmit] = useState(false);
    const [formulario, setFormulario] = useState(default_formulario_data);
    const [error_formulario, setErrorFormulario] = useState(error_formulario_data);
    const onChange_setFormulario = (event) => {
        console.log("onChange_setFormulario")
        //console.log(event)
        setFormulario({
            ...formulario,
            [event.target.name]: event.target.value
        });
        f_validation();
    }
    const clear = ()=>{
        console.log("clear")
        setFormulario(default_formulario_data)
    }
    const f_validation = async (event) => {
        console.log("ScreenTransfer f_validation")
        await setTimeout(function(){
            //console.log("error_formulario 0 ",error_formulario)
            let aux = false;
            if( /^[a-zA-Z0-9]{8}$/.test(String(formulario.toAccount.trim())) ){
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "toAccount": false
                }))
            }else{
                aux = true
                console.log("error 1")
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "toAccount": true
                }))
            }
            if( /^[0-9]{0,6}$/.test(String(formulario.amount.trim())) || parseFloat(formulario.amount)>100000){
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "amount": false
                }))
            }else{
                aux = true
                console.log("error 1")
                setErrorFormulario((oldState)=>({
                    ...oldState,
                    "amount": true
                }))
            }
            setSubmit(!aux)
            //console.log("error_formulario 1", error_formulario)
        }, 1000);
    }
    useEffect(() => {
        console.log("ScreenTransfer============useEffect")
        console.log(formulario)
    },[formulario]);
    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("=================handleSubmit")
    }
    return (
        <>
            <div>
                <div className="col-4 card size_card-4">
                    <p className="titlaCard grey center">Create new transfer</p>
                    <form onSubmit={handleSubmit}>
                        <p>Select origin account</p>
                        <select name="fromAccount" className="form_imput" name="cars" id="cars">
                            <option value="volvo">Volvo</option>
                            <option value="saab">Saab</option>
                            <option value="mercedes">Mercedes</option>
                            <option value="audi">Audi</option>
                        </select>
                        <p>Destination account</p>
                        <input className="form_imput" type="number" id="toAccount" name="toAccount" value={formulario.toAccount} placeholder="" onChange={onChange_setFormulario} onBlur={()=>f_validation()}/>
                        {error_formulario.toAccount && <span>error</span>}
                        <p>Amount</p>
                        <input className="form_imput" type="text" id="amount" name="amount" value={formulario.amount} placeholder="" onChange={onChange_setFormulario} onBlur={()=>f_validation()}/>
                        {error_formulario.amount && <span>error</span>}
                        <div className="center">
                            {
                                submit? <button className="btn-login" type="submit">Enter</button> : <button className="btn-login disabled" disabled type="submit">Enter</button>
                            }
                            <button className="btn-login btn-cancel" type="button" onClick={clear}>Cancel</button>
                        </div>
                    </form>
                </div>
                <div className="contenedor_chart">
                    <div className=" card2 center">
                        <Chartjsgroupby/>
                    </div>
                </div>
            </div>
        </>
    )
}
export default ScreenTransfer